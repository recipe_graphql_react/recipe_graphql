exports.typeDefs = `

scalar Date

type Recipe {
  _id: ID
  name: String!
  category: ID!
  description: String!
  instructions: String!
  likes: Int
  username: ID!
  createdAt: String
  updatedAt: String
}

type User {
  _id: ID
  username: String!
  email: String!
  password: String!
  favorites: [Recipe]
  createdAt: String
  updatedAt: String
}

type Category {
  _id: ID
  name: String!
  description: String
  createdAt: String
  updatedAt: String
}

type Token {
  token: String!
}

type Query {
  getAllRecipes: [Recipe]
  getAllCategories: [Category]
  getAllUsers: [User]

  getCurrentUser: User
  getRecipe(_id: ID!): Recipe
}

type Mutation {
  addUser(username: String!, email: String!, password: String!): User
  addCategory(name: String!, description: String): Category
  addRecipe(name: String!, description: String!, category: ID!, instructions: String!, username: ID!): Recipe

  signupUser(username: String!, email: String!, password: String!): Token
  signinUser(username: String!, password: String!): Token
}

`;