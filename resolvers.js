const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const createToken = (user, secret, expiresIn) => {
  const { username, email } = user;
  return jwt.sign({ username, email }, secret, { expiresIn })
}

exports.resolvers = {
  Query: {
    getAllUsers: async (root, args, { User }) => {
      const users = await User.find();
      return users;
    },
    getCurrentUser: async (root, args, { currentUser, User }) => {
      if (!currentUser) return null;
      const user = await User.findOne({ username: currentUser.username }).populate({
        path: 'favorites',
        model: 'Recipe'
      })

      return user;
    },
    getAllCategories: async (root, args, { Category }) => {
      const categories = await Category.find();
      return categories;
    },
    getAllRecipes: async (root, args, { Recipe }) => {
      const recipes = await Recipe
        .find()
        .populate({ path: 'username', model: 'User' })
        .populate('category')
        .exec();

      return recipes;
    },
    getRecipe: async (root, { _id }, { Recipe}) => {
      const recipe = await Recipe.findById(_id);
      return recipe;
    }
  },

  Mutation: {
    addCategory: async (root, { name, description }, { Category }) => {
      const newCategory = await new Category({
        name, description
      }).save();

      return newCategory;
    },
    addRecipe: async (root, { name, category, description, instructions, likes, username}, { Recipe }) => {
      const newRecipe = await new Recipe({
        name, category, description, instructions, likes, username
      }).save();

      return newRecipe;
    },
    signupUser: async (root, { username, email, password }, { User }) => {
      const user = await User.findOne({ username })

      if (user) throw new Error('User already exists');

      const newUser = await new User({
        username, email, password
      }).save();

      return { token: createToken(newUser, process.env.SECRET_APP, '1hr') };
    },
    signinUser: async (root, { username, password }, { User }) => {
      const user = await User.findOne({ username });
      if (!user) throw new Error('User not found.');

      const isValidPassword = await bcrypt.compare(password, user.password);
      if (!isValidPassword) throw new Error('Invalid Password');

      return { token: createToken(user, process.env.SECRET_APP, '1hr') }
    }
  }
}