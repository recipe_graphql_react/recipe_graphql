import { gql } from 'apollo-boost';

// Recipes Queries
export const GET_ALL_RECIPES = gql`
query {
  getAllRecipes {
    _id
    name
    category
    description
    instructions
    likes
    username
    createdAt
  }
}
`;

export const GET_RECIPE = gql`
query($_id: ID!) {
  getRecipe(_id: $_id) {
    _id
    name
    category
    description
    instructions
    createdAt
    likes
    username
  }
}
`;

// Recipes Mutations

// User Queries
export const GET_CURRENT_USER = gql`
  query {
    getCurrentUser {
      username
      email
    }
  }
`;

// User Mutations
export const SIGNUP_USER = gql`
mutation($username: String!, $email: String!, $password: String!) {
  signupUser(username: $username, email: $email, password: $password) {
    token
  }
}
`;

export const SIGNIN_USER = gql`
mutation($username: String!, $password: String!) {
  signinUser(username: $username, password: $password) {
    token
  }
}
`;