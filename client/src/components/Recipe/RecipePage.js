import React from 'react';
import { withRouter } from 'react-router-dom'
import { Query } from 'react-apollo';

import { GET_RECIPE } from '../../queries';

const RecipePage = ({ match }) => {
    const { _id } = match.params;

    return (
        <Query query={GET_RECIPE} variables={{ _id }}>
            {({ data, loading, error }) => {
                if (loading) return <div>Loading</div>
                if (error) return <div>Error</div>

                return (
                    <div className="App">
                        <h2>{ data.getRecipe.name }</h2>
                        <p><strong>Category</strong>: { data.getRecipe.catergory }</p>
                        <p><strong>Description</strong>: { data.getRecipe.description }</p>
                        <p><strong>Instructions</strong>: { data.getRecipe.instructions }</p>
                        <p><strong>likes</strong>: { data.getRecipe.likes }</p>
                        <p><strong>Created By</strong>: { data.getRecipe.username }</p>

                        <button>Like</button>
                    </div>
                )
            }}
        </Query>
    )
};

export default withRouter(RecipePage);