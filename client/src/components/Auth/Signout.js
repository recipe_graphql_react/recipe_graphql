import React from 'react';
import { ApolloConsumer } from 'react-apollo';
import { withRouter} from 'react-router-dom';

const Signout = ({ history }) => {
    const handleSignout = (client, history) => {
        localStorage.setItem('token', '');
        client.resetStore();
        history.push('/');
    }

    return (
        <ApolloConsumer>
            {client => {
                return <button onClick={() => handleSignout(client, history)}>Signout</button>;
            }}
        </ApolloConsumer>
    )
}

export default withRouter(Signout);